# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

BASE_PV=$(ver_cut 1-2)
MY_PV="${PV/_rc/-}"

DESCRIPTION="The enterprise-ready edition of LibreOffice"
HOMEPAGE="https://www.collaboraoffice.com/solutions/collabora-office/"
BASE_SRC_URI="https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-debian10"

S=${WORKDIR}

SRC_URI="
        ${BASE_SRC_URI}/collaboraoffice${BASE_PV}-ure_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-core_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-images_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraoffice${BASE_PV}_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-calc_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-draw_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-impress_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-writer_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-graphicfilter_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-ooofonts_${MY_PV}_amd64.deb
        ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-ooolinguistic_${MY_PV}_amd64.deb
        pdfimport? ( ${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-extension-pdf-import_${MY_PV}_amd64.deb )
"

LICENSE="|| ( LGPL-3 MPL-2 )"
SLOT=${BASE_PV}
KEYWORDS="amd64"
IUSE="pdfimport"

LANGUAGES_DICT="ar bg br ca cs da de el en es et fr gd gl gu he hi hr hu id is it lt lv nl no oc pl pt-br pt-pt ro ru sk sl sr sv te uk vi "
LANGUAGES_BASIS="ar as ast bg bn-in br ca-valencia ca cs cy da de el en-gb en:en-us es et eu fi fr ga gd gl gu he hi hr hu id is it ja km kn ko lt lv ml mr nb nl nn oc or pa-in pl pt-br pt ro ru sk sl sr-latn sr sv ta te tr uk vi zh-cn zh-tw "
for lang in ${LANGUAGES_BASIS}; do
        basispack="${BASE_SRC_URI}/collaboraofficebasis${BASE_PV}-${lang#*:}_${MY_PV}_amd64.deb"
        SRC_URI+=" l10n_${lang%:*}? ( ${basispack} )"
        IUSE+=" l10n_${lang%:*}"
done
for lang in ${LANGUAGES_DICT}; do
        dictpack="${BASE_SRC_URI}/collaboraoffice${BASE_PV}-dict-${lang#*:}_${MY_PV}_amd64.deb"
        SRC_URI+=" l10n_${lang%:*}? ( ${dictpack} )"
        IUSE+=" l10n_${lang%:*}"
done
unset lang dictpack basispack

inherit unpacker

src_install() {
        dodir /
        cp -R "${S}/opt" "${D}/" || die "install failed!"
} 
