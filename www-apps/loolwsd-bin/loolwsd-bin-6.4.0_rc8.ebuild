# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

BASE_PV=$(ver_cut 1-2)
MY_PV="${PV/_rc/-}"

DESCRIPTION="LibreOffice Online WebSocket Daemon"
HOMEPAGE="https://www.collaboraoffice.com/code/"
BASE_SRC_URI="https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-debian10"

S=${WORKDIR}

BRANDING="code-brand_${BASE_PV}-3_all.deb"
SRC_URI="branding? ( ${BASE_SRC_URI}/${BRANDING} )"
SRC_URI+=" ${BASE_SRC_URI}/loolwsd_${MY_PV}_amd64.deb"

DEPEND="www-apps/collaboraoffice-bin
        >=sys-libs/glibc-2.28
        >=sys-libs/libcap-2.10
        >=sys-devel/gcc-6.0
        >=sys-libs/pam-0.99.7.1
        dev-libs/libpcre:3
        >=media-libs/libpng-1.6.2:0/16
        >=dev-libs/poco-1.9.0
        >=dev-libs/openssl-1.1.0
        >=sys-libs/zlib-1.2.0
        >=sys-apps/init-system-helpers-1.18
        sys-apps/shadow
        media-libs/fontconfig
        x11-libs/libSM
        x11-libs/libXinerama
        x11-libs/libXrender
        media-libs/mesa
        net-print/cups
        app-arch/cpio
        x11-libs/libxcb"

LICENSE="MPL-2"
SLOT=$BASE_PV
KEYWORDS="amd64"
IUSE="+branding"

inherit unpacker

src_install() {
        cp -R "${S}/etc" "${D}/" || die "Install failed!"
        cp -R "${S}/lib" "${D}/" || die "Install failed!"
        cp -R "${S}/usr" "${D}/" || die "Install failed!"
}

pkg_postinst() {
        einfo
        einfo "Set permissions"
        einfo "# setcap cap_fowner,cap_mknod,cap_sys_chroot=ep /usr/bin/loolforkit"
        einfo "# setcap cap_sys_admin=ep /usr/bin/loolmount"
        einfo "# rm -rf /opt/lool"
        einfo "# mkdir -p /opt/lool/child-roots"
        einfo "# chown lool:lool /opt/lool"
        einfo "# chown lool:lool /opt/lool/child-roots"
        einfo "# loolwsd-systemplate-setup /opt/lool/systemplate /opt"
        einfo
        einfo "To debug run:"
        einfo "# sudo -u lool /usr/bin/loolwsd --version --o:sys_template_path=/opt/lool/systemplate --o:child_root_path=/opt/lool/child-roots --o:file_server_root_path=/usr/share/loolwsd --o:user_interface.mode=notebookbar"
        einfo
}
